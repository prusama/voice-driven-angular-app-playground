import { Injectable } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActionStrategy } from './action-strategy';
import { ChangeThemeStrategy } from './change-theme-strategy';
import { ChangeTitleStrategy } from './change-title-strategy';
import { SpeechSynthesizerService } from '../web-apis/speech-synthesizer.service';

@Injectable({
  providedIn: 'root',
})
export class ActionContext {
  private currentStrategy?: ActionStrategy;

  constructor(
    private changeThemeStrategy: ChangeThemeStrategy,
    private changeTitleStrategy: ChangeTitleStrategy,
    private titleService: Title,
    private speechSynthesizer: SpeechSynthesizerService
  ) {
    this.changeTitleStrategy.titleService = titleService;
  }

  // This will check the user input and it will change the strategy or run final answer or run action according to the user input
  processMessage(message: string, language: string): void {
    const msg = message.toLowerCase();
    // Check if user is giving initial command
    const hasChangedStrategy = this.hasChangedStrategy(msg, language);

    let isFinishSignal = false;
    if (!hasChangedStrategy) {
      // This will find out if user is ending the current command
      isFinishSignal = this.isFinishSignal(msg, language);
    }

    // If user did not changed strategy and did not ended the command, perform action
    if (!hasChangedStrategy && !isFinishSignal) {
      this.runAction(message, language);
    }
  }

  // Is called when the user is performing action
  runAction(input: string, language: string): void {
    if (this.currentStrategy) {
      this.currentStrategy.runAction(input, language);
    }
  }

  // Changes the strategy
  setStrategy(strategy: ActionStrategy | undefined): void {
    this.currentStrategy = strategy;
  }


  // Does the user input contains voice commands? If yes, then set the strategy and app will ask about details
  private hasChangedStrategy(message: string, language: string): boolean {
    let strategy: ActionStrategy | undefined;

    // Chose strategy
    if (message === this.changeThemeStrategy.getStartSignal(language)) {
      strategy = this.changeThemeStrategy;
    }
    if (message === this.changeTitleStrategy.getStartSignal(language)) {
      strategy = this.changeTitleStrategy;
    }

    // If strategy chosen, then app will ask about details
    if (strategy) {
      this.setStrategy(strategy);
      this.speechSynthesizer.speak(
        strategy.getInitialResponse(language),
        language
      );
      return true;
    }

    return false;
  }

  // Is called when user will end the command
  private isFinishSignal(message: string, language: string): boolean {
    if (
      message === this.changeThemeStrategy.getEndSignal(language) ||
      message === this.changeTitleStrategy.getEndSignal(language)
    ) {
      if (this.currentStrategy) {
        this.speechSynthesizer.speak(
          this.currentStrategy.getFinishResponse(language),
          language
        );
      }
      this.setStrategy(undefined);
      return true;
    }

    return false;
  }
}
